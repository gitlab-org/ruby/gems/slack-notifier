# frozen_string_literal: true

require File.expand_path("../lib/slack-messenger/version", __FILE__)

Gem::Specification.new do |s|
  s.name          = "slack-messenger"
  s.version       = Slack::Messenger::VERSION
  s.platform      = Gem::Platform::RUBY

  s.summary       = "A slim ruby wrapper for posting to slack webhooks"
  s.description   = " A slim ruby wrapper for posting to slack webhooks "
  s.authors       = ["Steven Sloan"]
  s.email         = ["stevenosloan@gmail.com"]
  s.homepage      = "https://gitlab.com/gitlab-org/slack-notifier"
  s.license       = "MIT"

  s.files         = Dir["{lib}/**/*.rb"]
  s.test_files    = Dir["spec/**/*.rb"]
  s.require_path  = "lib"

  s.add_dependency "re2", "~> 2.7", ">= 2.7.0"
end
